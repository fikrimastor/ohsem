<?php

namespace App\Http\Livewire;

use App\Models\FileUpload;
use Livewire\Component;
use Livewire\WithPagination;

class FileUploadIndex extends Component
{
    use WithPagination;
    public $files;

    public function mount()
    {
        $user = auth()->user();

        $this->files = $user->fileUploads->pluck('filename')->toArray();
    }

    public function render()
    {
        return view('livewire.file-upload-index', [
            'fileUploads' => FileUpload::whereIn('filename', $this->files)->paginate(25)
        ]);
    }
}
