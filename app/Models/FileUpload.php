<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FileUpload extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'file_uploads';

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function uploadedDate() : Attribute
    {
        return Attribute::get(
            get: fn () => $this->created_at->format('d/m/Y'),
        );
    }
}
