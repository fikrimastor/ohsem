<div>

    <div class="">
        <table class="w-full whitespace-nowrap text-left text-gray-500 dark:text-gray-400">
            <thead>
            <tr class="text-black-100 border-t border-b text-[14px] text-sm dark:text-black-100">
                <th class="w-1/12 p-2 pt-4 text-left">{{ __('ID') }}</th>
                <th class="w-1/6 p-2 pt-4 text-left">{{ __('File Name') }}</th>
                <th class="w-1/6 p-2 pt-4 text-left">{{ __('Document Date') }}</th>
                <th class="w-1/3 p-2 pt-4 text-left">{{ __('Uploader') }}</th>
                <th class="w-1/6 p-2 pt-4 text-left">{{ __('Size') }}</th>
                <th class="w-1/6 p-2 pt-4 text-left">{{ __('Created At') }}</th>
            </tr>
            </thead>

            <tbody>
            @php
                $style = '[#F7F8FA]';
            @endphp

            @forelse ($fileUploads as $key => $file)
                <tr class="bg-{{ $style }} border-b text-gray-500">
                    <td class="p-2">{{ $loop['id'] }}</td>
                    <td class="p-2">{{ $file['filename'] ?? '-' }}</td>

                    <td class="p-2">
                        {{ date('d/m/y', strtotime($file['doc_date'])) }}
                    </td>

                    <td class="flex items-center justify-between p-2">
                        <div>
                            {{ $file['user']['name'] }}
                            <br />
                            <small>
                                {{ $list['user']['email'] ?? '-' }}
                            </small>
                        </div>
                    </td>

                    <td class="p-2">
                        {{ $file['size'] }}
                    </td>

                    <td class="p-2">
                        {{ ($file['uploaded_date']) }}
                    </td>
                </tr>

                @php

                    if ($style == '[#F7F8FA]') {
                        $style = 'white';
                    } else {
                        $style = '[#F7F8FA]';
                    }
                @endphp
            @empty
                <tr>
                    <td colspan="7"
                        class="w-20 rounded-lg border border-red-300 bg-red-100 py-0.5 text-center text-xs font-medium text-red-800 lg:mx-0">
                        <div>
                            {{ __('No Data Available') }}
                        </div>
                    </td>
                </tr>

                @php
                    $style = 'white';
                @endphp
            @endforelse
            </tbody>
        </table>

        <div class="mt-2 flex flex-row">
            {{ $fileUploads->links() }}
        </div>
    </div>
</div>
